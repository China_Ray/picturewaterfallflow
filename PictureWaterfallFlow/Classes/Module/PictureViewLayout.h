//
//  PictureViewLayout.h
//  PictureWaterfallFlow
//
//  Created by Admin on 15/11/3.
//  Copyright © 2015年 yulei. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PictureViewLayoutDelegate <UICollectionViewDelegateFlowLayout>
@required
#pragma mark - 自定义
//列数量
- (NSInteger)collectionView:(UICollectionView *)collectionView
                     layout:(UICollectionViewLayout *)layout
   numberOfColumnsInSection:(NSInteger)section;
@end

@interface PictureViewLayout : UICollectionViewLayout

@property (assign, nonatomic) id<PictureViewLayoutDelegate> delegate;

@end
